from socket import socket, timeout
from threading import Thread
from socket_utils import *

@start_end
def kill_main(kill):
    print('you can stop the server by entering "/exit" then return')
    print('server takes up to 10 seconds to stop')
    while not kill[0]:
        cmd = input()
        if cmd == '/exit':
            kill[0] = True
        else:
            print('invalid!')

@start_end
def handle_client(server, clients, client_connection, client_id, kill):
    print(f'[handle {client_id}] started')
    while not kill[0]:
        try:
            msg = client_connection.recv(4096).decode()
        except timeout:
            continue
        if msg != '':
            print(f'[handle {client_id}] got msg: {msg!r}')
            # send to everyone else
            # coming from: client_id
            for i in range(len(clients)):
                if i != client_id:
                    clients[i].send(f'{client_id}: {msg}'.encode())
                    # 0: what's up?
                    # 1: what's up with you?

@start_end
def listen(server, clients, kill):
    while not kill[0]:
        try:
            (new_connection, address) = server.accept()
        except timeout:
            continue
        new_connection.settimeout(10)
        new_id = len(clients)
        print(f'[listen] got new client {new_id}')
        print(f'their ip is: {address}')
        clients.append(new_connection)
        Thread(target=handle_client, args=(server, clients, new_connection, new_id, kill)).start()

@start_end
def main():
    HOST = ''
    PORT = 50004
    server = socket()
    server.bind((HOST, PORT))
    server.listen(0)
    server.settimeout(10)
    kill = [False]
    clients = []
    Thread(target=kill_main, args=(kill,)).start()
    Thread(target=listen, args=(server, clients, kill)).start()

main()
