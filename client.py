from socket import socket, timeout
from socket_utils import *
from threading import Thread

@start_end
def receive(server, kill):
    while not kill[0]:
        try:
            msg = server.recv(4096).decode()
        except timeout:
            continue
        if msg != '':
            print(msg)

@start_end
def send(server, kill):
    while not kill[0]:
        cmd = input()
        if cmd.startswith('/'):
            if cmd[1:] == 'exit':
                kill[0] = True
            else:
                print('invalid!')
        else:
            server.send(cmd.encode())

@start_end
def main():
    HOST = '192.168.0.14'
    PORT = 50004
    server = socket()
    print('connecting to server...')
    server.connect((HOST, PORT))
    print('success! enter some text, then press return to send it to server.')
    print('or, you can enter "/exit" to exit the client.')
    print('remember to shut down server afterwards, by entering "/exit" there')
    server.settimeout(10)
    kill = [False]
    Thread(target=send, args=(server, kill)).start()
    Thread(target=receive, args=(server, kill)).start()

main()

